# Copyright (C) 2003-2020 Free Software Foundation, Inc.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCC; see the file COPYING3.  If not see
# <http://www.gnu.org/licenses/>.

# This file was written by Gaius Mulley (gaius.mulley@southwales.ac.uk)
# for GNU Modula-2.

if $tracelevel then {
    strace $tracelevel
}

# load support procs
load_lib gm2-torture.exp

set gm2src ${srcdir}/../m2

gm2_init_pim "${srcdir}/gm2/pim/run/pass"
gm2_link_with "-lm2pim -lm2iso -lpthread"


foreach testcase [lsort [glob -nocomplain $srcdir/$subdir/*.mod]] {
    set output [exec ../../gm2 -B../../ -g -c -I$srcdir/../m2/gm2-libs -I$srcdir/gm2/pim/run/pass -I$srcdir/../m2/gm2-compiler -I../m2/gm2-libs -I../m2/gm2-compiler -fpim $srcdir/gm2/pim/run/pass/sys.mod]

    # If we're only testing specific files and this isn't one of them, skip it.
    if ![runtest_file_p $runtests $testcase] then {
	continue
    }

    gm2-torture-execute $testcase "" "pass"
}
