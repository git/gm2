<!-- Creator     : groff version 1.20.1 -->
<!-- CreationDate: Fri Jan 14 19:57:50 2011 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="generator" content="groff -Thtml, see www.gnu.org">
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<meta name="Content-Style" content="text/css">
<link rel="stylesheet" type="text/css" href="texi2tr.css" />
<title>GNU Modula-2</title>
</head>

<body>
  <!-- site wide top running header menu -->

  <ul id="overview">
    <li class="home">
      HOME_HREF
    </li>
    <li class="release">
      12_HREF
    </li>

    <li>
      <a href="news.html" title="News">News</a>
    </li>
    <li>
      <a href="development.html" title="Development">Development</a>
    </li>
    <li>
      <a href="community.html" title="Community">Community</a>
    </li>
  </ul>

  <div id="header">
    <h1>
      <span style="display:inline-block; vertical-align:middle">
	<img src="200px-Heckert_GNU_white.png" alt="Heckert_GNU_white.png">
      </span>
      <span style="display:inline-block; vertical-align:middle; font-size: 50px">
	GNU Modula-2
      </span>
    </h1>

    <!-- homepage tab menu -->

    <div id="tabmenu">
      <ul id="tab">
	<li><a href="about.html"><span>About</span></a></li>
	<li class="selected"><a href="download.html"><span>Download</span></a></li>
	<li><a href="release.html"><span>Latest Release</span></a></li>
	<li><a href="license.html"><span>License</span></a></li>
	<li><a href="platforms.html"><span>Platforms</span></a></li>
	<li><a href="users.html"><span>Users</span></a></li>
      </ul>
    </div>
  </div>

<div id="page">
  <div class="plain">
    <h2>The home of the GNU Modula-2 compiler</h2>
    <p>GNU Modula-2 is about to go into the GCC tree for the
      development branch (GCC-12).  In time it will be back ported to
      GCC-11.  At the time of writing the gcc-11 graft is the stable release.
      <div>
	<span style="display:inline-block; vertical-align:middle; margin-left: 3%">
	  <img src="release.png" alt="users"</img></span>
	       <span style="display:inline-block; vertical-align:middle">
		 Stable release of gm2 (branch gm2-11)
<pre>git clone http://floppsie.comp.glam.ac.uk/gm2 gm2-floppsie
git checkout gm2-11
</pre>
		 <p>Documentation for GNU Modula-2 on 11_HREF.</p>
	       </span>
      </div>
      <br>
      <div>
	<span style="display:inline-block; vertical-align:middle; margin-left: 3%">
	  <img src="release.png" alt="users"</img></span>
	       <span style="display:inline-block; vertical-align:middle">
		 Older stable release of gm2 (branch gm2-10)
<pre>git clone http://floppsie.comp.glam.ac.uk/gm2 gm2-floppsie
git checkout gm2-10
</pre>
		 <p>Documentation for GNU Modula-2 on 10_HREF.</p>
	       </span>
      </div>
      <br>
      <div>
	<span style="display:inline-block; vertical-align:middle; margin-left: 3%">
	  <img src="develop.png" alt="installing"</img></span>
	       <span style="display:inline-block; vertical-align:middle">
		 Development branch (for gcc-12)
		 <pre>git clone git://gcc.gnu.org/git/gcc.git gcc-git
cd gcc-git
git checkout devel/modula-2
</pre>
		 <p>This is in the process of migrating into the GCC
		   tree.  The devel/modula-2 branch will be manually rebased.</p>
		 <p>Documentation for GNU Modula-2 on 12_HREF.</p>
	       </span>
	</span>
      </div>
      <br>
      <div>
	<span style="display:inline-block; vertical-align:middle; margin-left: 3%">
	  <img src="snapshot.png" alt="library"</img></span>
	       <span style="display:inline-block; vertical-align:middle">
		 Other
		 <a href="http://floppsie.comp.glam.ac.uk/download/c/">snapshots</a>.
	       </span>
      </div>
    </p>
    <p>Binaries can be found under
    the <a href="platforms.html">Platforms</a> tab.</p>
  </div>
</div>
</body>
</html>
