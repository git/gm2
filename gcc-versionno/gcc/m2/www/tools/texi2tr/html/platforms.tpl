<!-- Creator     : groff version 1.20.1 -->
<!-- CreationDate: Fri Jan 14 19:57:50 2011 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="generator" content="groff -Thtml, see www.gnu.org">
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<meta name="Content-Style" content="text/css">
<link rel="stylesheet" type="text/css" href="texi2tr.css" />
<title>GNU Modula-2</title>
</head>

<body>
  <!-- site wide top running header menu -->

  <ul id="overview">
    <li class="home">
      HOME_HREF      
    </li>
    <li class="release">
      12_HREF
    </li>

    <li>
      <a href="news.html" title="News">News</a>
    </li>
    <li>
      <a href="development.html" title="Development">Development</a>
    </li>
    <li>
      <a href="community.html" title="Community">Community</a>
    </li>
  </ul>

  <div id="header">
    <h1>
      <span style="display:inline-block; vertical-align:middle">
	<img src="200px-Heckert_GNU_white.png" alt="Heckert_GNU_white.png">
      </span>
      <span style="display:inline-block; vertical-align:middle; font-size: 50px">
	GNU Modula-2
      </span>
    </h1>

    <!-- homepage tab menu -->

    <div id="tabmenu">
      <ul id="tab">
	<li><a href="about.html"><span>About</span></a></li>
	<li><a href="download.html"><span>Download</span></a></li>
	<li><a href="release.html"><span>Latest Release</span></a></li>
	<li><a href="license.html"><span>License</span></a></li>
	<li class="selected"><a href="platforms.html"><span>Platforms</span></a></li>
	<li><a href="users.html"><span>Users</span></a></li>
      </ul>
    </div>    
  </div>

<div id="page">
  <div class="plain">
    <h2>The home of the GNU Modula-2 compiler</h2>
    <p>GNU Modula-2 is about to go into the GCC tree and therefore it is likely
      it can be installed via your distribution repository.  </p>
    <p>If your distribution does not include gm2 then you can build it
      from source alternatively for a few distributions there are some
      pre-built binararies.</p>

    <table width="100%">
	<tr valign="top">
	  <td style="width:40%;text-align:top;">
	    <div>
	      <p>
		<span style="display:inline-block; vertical-align:middle; margin-left: 3%">
		  <img src="debian-swirl48x48.png" alt="debian"</img></span>
		       <span style="display:inline-block; vertical-align:middle">
			 <a href="debian.html">Debian
			   packages</a>.
	      </span></p>
	    </div>
	    <div>
	      <p><span style="display:inline-block; vertical-align:middle; margin-left: 3%">
		  <img src="terminal.png" alt="./configure etc"</img></span>
		       <span style="display:inline-block; vertical-align:middle">
			 <a href="building.html">Building
			   on GNU/Linux</a>.
	      </span></p>
	    </div>
	  </td>
	  <td style="width:40%;text-align:top;">
	    <div>
	    </div>
	    <div>
	    </div>
	    <div>
	    </div>
	  </td>
	</tr>
      </table>
    </p>
  </div>
</div>
</body>
</html>
